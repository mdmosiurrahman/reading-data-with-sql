﻿using System;
using System.Collections.Generic;
using Assignment6.Models;
using Assignment6.Repositories;

namespace Assignment6
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            IInvoiceRepository invoiceRepository = new InvoiceRepository();
            IGenre genreRepository = new GenreRepository();

            // Print customers on a page with Limit and Offset parameters(success)
            //TestGetAllCustomers(repository);

            //Return the number of customers in Each country in descending order
            //TestGetNoofCustomersByCountry(repository);

            //GetCustomer By  Id (Success)
            // TestGetCustomerById(repository);

            //Get Customer By Name Using Like:(success)

            //TestGetCustomerByName(repository);

            //Add new customer to the Database
            //TestInsertCustomer(repository);

            //update an existing customer by Id
            // TestUpdateCustomer(repository);

            //Customers who are the spenders(Total in invoice is the largest)
            // TestHighestSpenders(invoiceRepository);

            //Return the Customers page using Limit and Offset parameters
            //TestPrintLimitedCustomers(repository);

            //Get Customers with Most popular Genre Count
            //TestPrintCustomersMostPopularGenre(genreRepository);

            Console.ReadKey();
        }
        static void TestHighestSpenders(IInvoiceRepository repository)
        {
            PrinthighestSpenders(repository.GetHighestSpenders());
        }
        static void TestGetCustomerById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById(2));
        }
        static void TestGetCustomerByName(ICustomerRepository repository)
        {
            PrintAllCustomers(repository.GetCustomerByName("osi"));

        }
        static void TestPrintCustomersMostPopularGenre(IGenre repository)
        {
            PrintCustomersMostPopularGenre(repository.CustomerMostPopularGenre("mosiur"));
        }
        static void TestGetAllCustomers(ICustomerRepository repository)
        {
            PrintAllCustomers(repository.GetAllCustomers());
        }
        static void TestInsertCustomer(ICustomerRepository repository)
        {
            Customer temp = new Customer()
            {
                FirstName = "Ram",
                LastName = "Pal",
                Country = "India",
                PostalCode = "5000798",
                PhoneNumber = "3265984",
                Email = "abc@abc.com",

            };
            if (repository.AddNewCustomer(temp))
            {
                Console.WriteLine("------Customer Added----------");
            }

        }
        static void TestUpdateCustomer(ICustomerRepository repository)
        {

            Customer temp = new Customer()
            {
                CustomerId = 3,
                FirstName = "Mosiur",
                LastName = "Rahman",
                Country = "Bangla",
                PostalCode = "5000798",
                PhoneNumber = "3265984",
                Email = "abc@abc.com",

            };
            if (repository.UpdateCustomerById(temp))
            {
                Console.WriteLine("------Customer Updated----------");
            }
        }
        static void TestGetNoofCustomersByCountry(ICustomerRepository repository)
        {
            PrintCustomersByCountry(repository.GetCountCustomersByCountry());
        }
        static void TestPrintLimitedCustomers(ICustomerRepository repository)
        {
            PrintLimitedCustomers(repository.GetLimitedcustomers(3, 5));
        }
        static void PrintAllCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        private static void PrintLimitedCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        private static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"---{customer.CustomerId} {customer.FirstName}  {customer.LastName}   {customer.Country}  {customer.PostalCode}   {customer.PhoneNumber}   {customer.Email}---");
        }
        static void PrintCustomersMostPopularGenre(IEnumerable<Genre> genres)
        {
            foreach (Genre genre in genres)
            {
                PrintCustomerMostPopularGenre(genre);
            }
        }
        private static void PrintCustomerMostPopularGenre(Genre genre)
        {
            Console.WriteLine($"----{genre.CustomerFirstName} {genre.CustomerLastName}  {genre.GenreName} {genre.GenreCount}----- ");
        }
        static void PrintCustomersByCountry(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                printCustomerByCountry(customer);
            }
        }
        private static void printCustomerByCountry(Customer customerByCountry)
        {
            Console.WriteLine($"---{customerByCountry.NoOfCustomersByCountry} {customerByCountry.Country}  ---");

        }
        static void PrinthighestSpenders(IEnumerable<Invoice> HighestSpenders)
        {
            foreach (Invoice highestspender in HighestSpenders)
            {
                PrintHighestSpender(highestspender);

            }
        }
        private static void PrintHighestSpender(Invoice highestSpender)
        {
            Console.WriteLine($"----{highestSpender.CustomerId}  {highestSpender.InvoiceId} {highestSpender.FirstName}  {highestSpender.LastName} {highestSpender.Total}---");

        }

    }
}
