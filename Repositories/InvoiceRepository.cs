﻿using Assignment6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Repositories
{
    class InvoiceRepository : IInvoiceRepository
    {
        public List<Invoice> GetHighestSpenders()
        {
            List<Invoice> customers = new List<Invoice>();
            string sql = "SELECT Customer.CustomerId,Customer.FirstName,Customer.LastName,Invoice.InvoiceId,Invoice.Total FROM Customer" +
                " INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId Order by Invoice.Total desc";

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Invoice temp = new Invoice();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.InvoiceId = reader.GetInt32(3);
                                temp.Total = reader.GetDecimal(4);
                                customers.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
    }
}
