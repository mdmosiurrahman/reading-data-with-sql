USE SuperherosDb;

INSERT INTO Superhero(Name, Alias, Origin)
VALUES('Batman', 'Bat on chest', 'USA');
INSERT INTO Superhero(Name, Alias, Origin)
VALUES('Superman', 'S on chest', 'UK');
INSERT INTO Superhero(Name, Alias, Origin)
VALUES('Spiderman', 'Spider on chest', 'Denmark');
